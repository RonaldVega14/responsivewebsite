**Responsive Website**
This is a complete responsive website made with HTML, JS and CSS.

**Screenshots**

<img src="screenshots/home.PNG" width = "640"><br>

<img src="screenshots/home2.PNG" width = "640"><br>

<img src="screenshots/home3.PNG" width = "640"><br>

<img src="screenshots/home4.PNG" width = "640"><br>

<img src="screenshots/home5.PNG" width = "640"><br>

<img src="screenshots/home6.PNG" width = "640"><br>

- Services Page

<img src="screenshots/services.PNG" width = "640"><br>